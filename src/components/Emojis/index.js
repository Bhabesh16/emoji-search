import React from 'react';
import PropTypes from "prop-types";
import Clipboard from 'clipboard';

import './Emojis.css';
import Emoji from '../Emoji';
import NoEmoji from '../NoEmoji';

class Emojis extends React.Component {

  static propTypes = {
    emojiData: PropTypes.array
  }

  componentDidMount() {
    this.clipboard = new Clipboard(".copy-to-clipboard");
  }

  componentWillUnmount() {
    this.clipboard.destroy();
  }

  render() {
    return (
      <div className='main-container'>
        <div className='emojis-container'>
          {this.props.emojiData.length > 0 ?
            this.props.emojiData.map((emoji) => {
              return (
                <Emoji
                  key={emoji.title}
                  symbol={emoji.symbol}
                  title={emoji.title}
                />
              );
            }) :
            <NoEmoji />
          }
        </div>
      </div>
    );
  }
}

export default Emojis;