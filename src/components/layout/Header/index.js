import React from 'react';

import sunglassEmoji from '../../../assets/gifs/sunglass-emoji.gif';
import laughingEmoji from '../../../assets/gifs/laughing-emoji.gif';
import './Header.css';

class Header extends React.Component {
    render() {
        return (
            <div className="header-container">
                <img
                    src={sunglassEmoji}
                    alt=""
                />
                Emoji Search
                <img
                    src={laughingEmoji}
                    alt=""
                />
            </div>
        );
    }
}

export default Header;