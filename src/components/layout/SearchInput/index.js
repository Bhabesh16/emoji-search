import React from 'react';
import PropTypes from "prop-types";

import magnifier from '../../../assets/images/magnifier.png';
import './SearchInput.css';

class SearchInput extends React.Component {

    static propTypes = {
        searchEmoji: PropTypes.func
    };

    handleChange = (event) => {
        this.props.searchEmoji(event);
    };

    render() {
        return (
            <div className='main-container'>
                <div className='input-container'>
                    <img
                        src={magnifier}
                        alt=""
                    />
                    <input
                        placeholder='Search emojis...'
                        onChange={this.handleChange}
                    />
                </div>
            </div>
        );
    }
}

export default SearchInput;