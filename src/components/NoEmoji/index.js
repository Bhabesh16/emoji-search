import React from 'react';

import thinkingEmoji from "../../assets/gifs/thinking-emoji.gif";
import './NoEmoji.css';

class NoEmoji extends React.Component {
    render() {
        return (
            <div className='no-emoji-container'>
                <img
                    src={thinkingEmoji}
                    alt=""
                />
                <span className='no-image-title'>No Emojis Found</span>
                <span>There are no emojis that match your current filters. Try removing some of them to get better results.</span>
            </div>
        );
    }
}

export default NoEmoji;