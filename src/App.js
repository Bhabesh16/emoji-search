import React from 'react';

import './App.css';
import filteredEmojis from './filteredEmojis';
import Header from './components/layout/Header';
import SearchInput from './components/layout/SearchInput';
import Emojis from './components/Emojis';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      filteredEmojis: filteredEmojis("")
    };
  }

  searchEmoji = (event) => {
    this.setState({
      filteredEmojis: filteredEmojis(event.target.value)
    });
  };

  render() {
    return (
      <div className='container'>
        <Header />
        <SearchInput searchEmoji={this.searchEmoji} />
        <Emojis emojiData={this.state.filteredEmojis} />
      </div>
    );
  }
}

export default App;