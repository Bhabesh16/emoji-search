import emojis from "./assets/emojiData/emojis.json";

function filteredEmojis(searchText) {

    const filterEmojis = emojis.filter((emoji) => {

        if (emoji.title.toLowerCase().includes(searchText.toLowerCase())) {
            return true;
        } else if (emoji.keywords.includes(searchText.toLowerCase())) {
            return true;
        } else if (emoji.symbol.includes(searchText)) {
            return true;
        } else {
            return false;
        }
    });

    if (searchText === "") {
        return filterEmojis.slice(0, 36);
    } else {
        return filterEmojis;
    }
}

export default filteredEmojis;